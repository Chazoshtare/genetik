package com.sneknology.genetik

import com.sneknology.genetik.listener.LoggingListener
import com.sneknology.genetik.model.Experiment
import com.sneknology.genetik.model.Individual
import com.sneknology.genetik.processes.*
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.junit.jupiter.api.Test
import kotlin.random.Random

private val logger = KotlinLogging.logger { }

internal class SampleRun {

    @Test
    fun `runWithBooleans should finish after 40 generations`() {
        val experiment = Experiment(
            "Sample Run",
            40,
            { Individual((1..20).map { Random.nextBoolean() }) },
            { it.genotype.count { gene -> gene } },
            tournamentSelection(3, 25, 2),
            onePointCrossover(0.8),
            booleanInversionMutation(0.01),
            maxGenerationsTermination(40)
        )

        val runner = ExperimentRunner<List<Boolean>>()
        runBlocking {
            val finalPopulation = runner.run(experiment, LoggingListener())
            logger.info { finalPopulation.maxByOrNull { it.fitness!! } }
        }
    }

    @Test
    fun `runWithIntegers should finish after 100 generations`() {
        val geneProducer = { Random.nextInt(50) }
        val experiment = Experiment(
            "Sample Run",
            100,
            { Individual((1..20).map { geneProducer() }) },
            { it.genotype.sum() },
            truncationSelection(70, 3),
            twoPointCrossover(0.8),
            geneReplacementMutation(geneProducer, 0.01),
            maxGenerationsTermination(100)
        )

        val runner = ExperimentRunner<List<Int>>()
        runBlocking {
            val finalPopulation = runner.run(experiment, LoggingListener())
            logger.info { finalPopulation.maxByOrNull { it.fitness!! } }
        }
    }
}
