package com.sneknology.genetik.processes

import com.sneknology.genetik.model.ExperimentState
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class TerminationsTest {

    @Test
    fun `maxGenerationsTermination should not terminate if experiment did not reach max`() {
        // given
        val state = ExperimentState<Int>(49, listOf(), listOf())
        val termination = maxGenerationsTermination(50)

        // when
        val shouldTerminate = termination(state)

        // then
        assertFalse(shouldTerminate)
    }

    @Test
    fun `maxGenerationsTermination should terminate if experiment reached max`() {
        // given
        val state = ExperimentState<Int>(50, listOf(), listOf())
        val termination = maxGenerationsTermination(50)

        // when
        val shouldTerminate = termination(state)

        // then
        assertTrue(shouldTerminate)
    }

    @Test
    fun `steadyFitnessTermination should not terminate if fitness was not steady for enough generations`() {
        // given
        val state = ExperimentState<Int>(1, listOf(), listOf(5, 5, 5, 10, 10, 10, 10))
        val termination = steadyFitnessTermination(5)

        // when
        val shouldTerminate = termination(state)

        // then
        assertFalse(shouldTerminate)
    }

    @Test
    fun `steadyFitnessTermination should terminate if fitness was steady for enough generations`() {
        // given
        val state = ExperimentState<Int>(1, listOf(), listOf(5, 5, 10, 10, 10, 10, 10))
        val termination = steadyFitnessTermination(5)

        // when
        val shouldTerminate = termination(state)

        // then
        assertTrue(shouldTerminate)
    }
}
