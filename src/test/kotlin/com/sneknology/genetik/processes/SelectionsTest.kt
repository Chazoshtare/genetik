package com.sneknology.genetik.processes

import com.sneknology.genetik.model.Individual
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class SelectionsTest {

    @Test
    fun `truncationSelection should select the fittest individuals`() {
        // given
        val selection = truncationSelection<String>(3, 0)
        val population = listOf(
            Individual("", 10),
            Individual("", 30),
            Individual("", 60),
            Individual("", 40),
            Individual("", 20),
            Individual("", 50),
        )

        // when
        val selectedPopulation = selection.selector(population)

        // then
        val expectedPopulation = listOf(
            Individual("", 50),
            Individual("", 40),
            Individual("", 60)
        )
        assertEquals(3, selectedPopulation.size)
        assertTrue(selectedPopulation.containsAll(expectedPopulation))
    }

    @Test
    fun `tournamentSelection should select the best individual`() {
        // given
        val selection = tournamentSelection<String>(5, 50, 0)
        val population = listOf(
            Individual("", 10),
            Individual("", 30),
            Individual("", 40),
            Individual("", 50),
            Individual("", 20),
        )

        // when
        val selectedPopulation = selection.selector(population)

        // then
        assertEquals(50, selectedPopulation.size)
        assertTrue(selectedPopulation.all { it.fitness!! == 50 })
    }

    @Test
    fun `tournamentSelection should never select the worst individuals`() {
        // given
        val selection = tournamentSelection<String>(3, 50, 0)
        val population = listOf(
            Individual("", 10),
            Individual("", 30),
            Individual("", 60),
            Individual("", 40),
            Individual("", 20),
            Individual("", 50),
        )

        // when
        val selectedPopulation = selection.selector(population)

        // then
        assertEquals(50, selectedPopulation.size)
        assertTrue(selectedPopulation.all { it.fitness!! >= 30 })
    }
}
