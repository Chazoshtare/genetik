package com.sneknology.genetik.processes

import com.sneknology.genetik.model.Individual
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class MutationsTest {

    @Test
    fun `booleanInversionMutation should invert all genes`() {
        // given
        val mutation = booleanInversionMutation(1.0)
        val individual = Individual(listOf(true, true, false, false, true))

        // when
        val resultingIndividual = mutation(individual)

        // then
        assertEquals(listOf(false, false, true, true, false), resultingIndividual.genotype)
    }

    @Test
    fun `geneReplacementMutation should replace all genes`() {
        // given
        val mutation = geneReplacementMutation({ 20 }, 1.0)
        val individual = Individual(listOf(1, 2, 3, 4, 5))

        // when
        val resultingIndividual = mutation(individual)

        // then
        assertEquals(listOf(20, 20, 20, 20, 20), resultingIndividual.genotype)
    }

    @Test
    fun `geneModificationMutation should modify all genes`() {
        // given
        val mutation = geneModificationMutation<Int>({ it * 10 }, 1.0)
        val individual = Individual(listOf(1, 2, 3, 4, 5))

        // when
        val resultingIndividual = mutation(individual)

        // then
        assertEquals(listOf(10, 20, 30, 40, 50), resultingIndividual.genotype)
    }

    @Test
    fun `noMutation should leave the same genotype`() {
        // given
        val mutation = noMutation()
        val individual = Individual(listOf(1, 2, 3, 4, 5))

        // when
        val resultingIndividual = mutation(individual)

        // then
        assertEquals(listOf(1, 2, 3, 4, 5), resultingIndividual.genotype)
    }
}
