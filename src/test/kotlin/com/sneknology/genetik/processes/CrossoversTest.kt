package com.sneknology.genetik.processes

import com.sneknology.genetik.model.Individual
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class CrossoversTest {

    @Test
    fun `onePointCrossover should cross two individuals and leave the same gene pool`() {
        // given
        val crossover = onePointCrossover<Int>(1.0)
        val firstIndividual = Individual(listOf(5, 10, 15, 20, 25))
        val secondIndividual = Individual(listOf(3, 6, 9, 12, 15))
        val initialGeneSum = firstIndividual.genotype.sum() + secondIndividual.genotype.sum()

        // when
        val (firstResulting, secondResulting) = crossover.recombiner(firstIndividual, secondIndividual)
        val resultingGeneSum = firstResulting.genotype.sum() + secondResulting.genotype.sum()

        // then
        assertAll(
            { assertNotEquals(firstIndividual, firstResulting) },
            { assertNotEquals(secondIndividual, secondResulting) },
            { assertEquals(resultingGeneSum, initialGeneSum) }
        )
    }

    @Test
    fun `twoPointCrossover should cross two individuals and leave the same gene pool`() {
        // given
        val crossover = twoPointCrossover<Int>(1.0)
        val firstIndividual = Individual(listOf(5, 10, 15, 20, 25))
        val secondIndividual = Individual(listOf(3, 6, 9, 12, 15))
        val initialGeneSum = firstIndividual.genotype.sum() + secondIndividual.genotype.sum()

        // when
        val (firstResulting, secondResulting) = crossover.recombiner(firstIndividual, secondIndividual)
        val resultingGeneSum = firstResulting.genotype.sum() + secondResulting.genotype.sum()

        // then
        assertAll(
            { assertNotEquals(firstIndividual, firstResulting) },
            { assertNotEquals(secondIndividual, secondResulting) },
            { assertEquals(resultingGeneSum, initialGeneSum) }
        )
    }

    // TODO: tests for shortest possible lists
}
