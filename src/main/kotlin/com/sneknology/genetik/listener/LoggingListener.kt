package com.sneknology.genetik.listener

import mu.KotlinLogging

private val logger = KotlinLogging.logger { }

class LoggingListener : ExperimentListener {

    override fun onStart() {
        logger.info { "Experiment started." }
    }

    override fun onNewGeneration(generation: Int) {
        logger.info { "Evaluating generation $generation" }
    }

    override fun onFinish() {
        logger.info { "Experiment finished." }
    }
}
