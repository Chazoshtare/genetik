package com.sneknology.genetik.listener

interface ExperimentListener {

    fun onStart() {}
    fun onNewGeneration(generation: Int) {}
    fun onFinish() {}
}
