package com.sneknology.genetik.processes

import com.sneknology.genetik.model.ExperimentState

fun maxGenerationsOrSteadyFitnessTermination(
    maxGenerations: Int,
    steadyForGenerationsNumber: Int
): (ExperimentState<*>) -> Boolean =
    {
        maxGenerationsTermination(maxGenerations).invoke(it) &&
                steadyFitnessTermination(steadyForGenerationsNumber).invoke(it)
    }

fun maxGenerationsTermination(maxGenerations: Int): (ExperimentState<*>) -> Boolean =
    { it.currentGeneration >= maxGenerations }

fun steadyFitnessTermination(steadyForGenerationsNumber: Int): (ExperimentState<*>) -> Boolean =
    {
        val lastFitnesses = it.topFitnessHistory.takeLast(steadyForGenerationsNumber)
        lastFitnesses.size == steadyForGenerationsNumber && lastFitnesses.distinct().size == 1
    }
