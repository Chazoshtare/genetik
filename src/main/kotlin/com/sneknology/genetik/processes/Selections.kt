package com.sneknology.genetik.processes

import com.sneknology.genetik.model.Selection

fun <T> truncationSelection(
    survivorsCount: Int,
    keepElite: Int = 0
) = Selection<T>({ it.sortedByDescending { individual -> individual.fitness }.subList(0, survivorsCount) }, keepElite)

fun <T> tournamentSelection(
    individualsInTournament: Int,
    tournamentCount: Int,
    keepElite: Int = 0
) = Selection<T>(
    {
        (1..tournamentCount).map { _ ->
            it.shuffled().subList(0, individualsInTournament).maxByOrNull { individual -> individual.fitness!! }!!
        }
    },
    keepElite
)
