package com.sneknology.genetik.processes

import com.sneknology.genetik.model.Crossover
import com.sneknology.genetik.model.Individual
import kotlin.random.Random

fun <T> onePointCrossover(probability: Double = 0.8) = Crossover<List<T>>(::onePointRecombiner, probability)

fun <T> twoPointCrossover(probability: Double = 0.8) = Crossover<List<T>>(::twoPointRecombiner, probability)

private fun <T> onePointRecombiner(
    first: Individual<List<T>>,
    second: Individual<List<T>>
): Pair<Individual<List<T>>, Individual<List<T>>> {
    require(first.genotype.size >= 2 && second.genotype.size >= 2) {
        "Genotype size should be at least 2 for one point crossover"
    }

    val cut = Random.nextInt(1, first.genotype.size)
    return crossListGenotypeIndividuals(first, second, 0, cut)
}

private fun <T> twoPointRecombiner(
    first: Individual<List<T>>,
    second: Individual<List<T>>
): Pair<Individual<List<T>>, Individual<List<T>>> {
    require(first.genotype.size >= 3 && second.genotype.size >= 3) {
        "Genotype size should be at least 3 for two point crossover"
    }

    val firstCut = Random.nextInt(1, first.genotype.size - 1)
    val secondCut = Random.nextInt(firstCut + 1, first.genotype.size)

    return crossListGenotypeIndividuals(first, second, firstCut, secondCut)
}

private fun <T> crossListGenotypeIndividuals(
    first: Individual<List<T>>,
    second: Individual<List<T>>,
    firstCutPosition: Int,
    secondCutPosition: Int
): Pair<Individual<List<T>>, Individual<List<T>>> {
    check(first.genotype.size == second.genotype.size) { "Both genotypes should have the same size!" }
    val firstGenotype = first.genotype
    val secondGenotype = second.genotype

    val firstResultingIndividual = Individual(
        firstGenotype.subList(0, firstCutPosition) +
                secondGenotype.subList(firstCutPosition, secondCutPosition) +
                firstGenotype.subList(secondCutPosition, secondGenotype.size)
    )
    val secondResultingIndividual = Individual(
        secondGenotype.subList(0, firstCutPosition) +
                firstGenotype.subList(firstCutPosition, secondCutPosition) +
                secondGenotype.subList(secondCutPosition, secondGenotype.size)
    )

    return firstResultingIndividual to secondResultingIndividual
}
