package com.sneknology.genetik.processes

import com.sneknology.genetik.model.Individual
import kotlin.random.Random

fun booleanInversionMutation(probability: Double) = geneModificationMutation<Boolean>({ !it }, probability)

fun <T> geneReplacementMutation(
    geneProducer: () -> T,
    probability: Double
): (Individual<List<T>>) -> Individual<List<T>> =
    { Individual(it.genotype.map { gene -> if (Random.nextDouble() < probability) geneProducer() else gene }) }

fun <T> geneModificationMutation(
    geneModifier: (T) -> T,
    probability: Double
): (Individual<List<T>>) -> Individual<List<T>> =
    { Individual(it.genotype.map { gene -> if (Random.nextDouble() < probability) geneModifier(gene) else gene }) }

fun noMutation(): (Individual<*>) -> Individual<*> = { it }
