package com.sneknology.genetik.model

/**
 * Definition of an entire experiment.
 */
data class Experiment<T>(
    val name: String,
    val populationSize: Int,
    val individualProducer: () -> Individual<T>,
    val evaluation: suspend (Individual<T>) -> Int,
    val selection: Selection<T>,
    val crossover: Crossover<T>,
    val mutation: (Individual<T>) -> Individual<T> = { it },
    val termination: (ExperimentState<T>) -> Boolean
) {

    fun validate() {
        if (populationSize < 1 || selection.keepElite >= populationSize) {
            throw IllegalArgumentException("Invalid Experiment parameters!")
        }
    }

    fun shouldTerminate(state: ExperimentState<T>) = termination(state)
}

/**
 * Definition of the selection process in an experiment. Determines which individuals will be used to create a next
 * generation.
 *
 * The [selector] receives an entire population sorted descending by fitness and is expected to only return
 * the individuals that should be used during the crossover process.
 *
 * [keepElite] is a number of the best individuals that will be kept between generations.
 */
class Selection<T>(
    val selector: (List<Individual<T>>) -> List<Individual<T>>,
    val keepElite: Int = 0
)

/**
 * Definition of the crossover process in an experiment. Determines how are individuals crossed to create a next
 * generation.
 *
 * The [recombiner] receives random two previously selected individuals and returns two newly created ones.
 *
 * [probability] defines the actual probability of a crossover taking place for these two individuals, in range 0-1
 * (effectively meaning 0%-100%).
 */
class Crossover<T>(
    val recombiner: (Individual<T>, Individual<T>) -> Pair<Individual<T>, Individual<T>>,
    val probability: Double = 0.8
)
