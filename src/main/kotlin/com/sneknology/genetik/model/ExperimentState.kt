package com.sneknology.genetik.model

data class ExperimentState<T>(
    val currentGeneration: Int,
    val currentPopulation: List<Individual<T>>,
    val topFitnessHistory: List<Int>
)
