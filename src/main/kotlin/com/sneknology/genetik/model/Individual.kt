package com.sneknology.genetik.model

data class Individual<T>(
    val genotype: T,
    val fitness: Int? = null
)
