package com.sneknology.genetik

import com.sneknology.genetik.listener.ExperimentListener
import com.sneknology.genetik.listener.NoOpListener
import com.sneknology.genetik.model.Crossover
import com.sneknology.genetik.model.Experiment
import com.sneknology.genetik.model.ExperimentState
import com.sneknology.genetik.model.Individual
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlin.random.Random

class ExperimentRunner<T> {

    /**
     * Runs a given [experiment] and returns the last generation's evaluated population sorted descending by fitness.
     */
    suspend fun run(experiment: Experiment<T>, listener: ExperimentListener = NoOpListener()): List<Individual<T>> {
        experiment.validate()
        listener.onStart()
        with(experiment) {
            var population = (1..populationSize).map { individualProducer() }
            val topFitnessHistory = mutableListOf<Int>()
            var currentGeneration = 1

            while (true) {
                val experimentState = ExperimentState(currentGeneration, population, topFitnessHistory)

                listener.onNewGeneration(currentGeneration)
                val evaluatedPopulation = population.evaluate(evaluation)
                    .sortedByDescending { it.fitness }

                topFitnessHistory.add(evaluatedPopulation[0].fitness!!)

                if (!experiment.shouldTerminate(experimentState)) {
                    val selectedIndividuals = selection.selector(evaluatedPopulation)
                    val crossedIndividuals = crossAllIndividuals(selectedIndividuals, crossover)
                    val mutatedIndividuals = crossedIndividuals.map(mutation)

                    population = mutatedIndividuals +
                            evaluatedPopulation.subList(0, selection.keepElite) +
                            (mutatedIndividuals.size until (populationSize - selection.keepElite))
                                .map { individualProducer() }
                    currentGeneration++
                } else {
                    listener.onFinish()
                    return evaluatedPopulation
                }
            }
        }
    }

    private fun <T> crossAllIndividuals(
        selectedIndividuals: List<Individual<T>>,
        crossover: Crossover<T>
    ): List<Individual<T>> =
        selectedIndividuals.shuffled()
            .chunked(2)
            .filter { it.size == 2 }
            .map { cross(it.first() to it.last(), crossover) }
            .flatMap { listOf(it.first, it.second) }

    private fun <T> cross(individuals: Pair<Individual<T>, Individual<T>>, crossover: Crossover<T>) =
        if (Random.nextDouble() < crossover.probability) {
            crossover.recombiner(individuals.first, individuals.second)
        } else {
            individuals
        }

    private suspend fun List<Individual<T>>.evaluate(evaluator: suspend (Individual<T>) -> Int): List<Individual<T>> =
        coroutineScope {
            map {
                async { Individual(it.genotype, evaluator(it)) }
            }.awaitAll()
        }
}
