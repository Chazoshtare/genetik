plugins {
    kotlin("jvm") version "1.8.21"
    `maven-publish`
}

group = "com.sneknology"
version = "0.0.1"

repositories {
    mavenCentral()
}

dependencies {
    // @formatter:off
    val coroutinesVersion = "1.6.4" // https://mvnrepository.com/artifact/org.jetbrains.kotlinx/kotlinx-coroutines-core
    val kotlinLoggingVersion = "3.0.5" // https://mvnrepository.com/artifact/io.github.microutils/kotlin-logging
    val logbackVersion = "1.4.7" // https://mvnrepository.com/artifact/ch.qos.logback/logback-classic
    // @formatter:on

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    implementation("io.github.microutils:kotlin-logging:$kotlinLoggingVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")

    testImplementation("org.jetbrains.kotlin:kotlin-test:1.8.21")
}

tasks.test {
    useJUnitPlatform()
}

tasks.compileKotlin {
    kotlinOptions.jvmTarget = "17"
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}
